#include <iostream>
#include "pessoa.hpp"

using namespace std;

int main(int argc, char ** argv) {

   Pessoa pessoa_1;
   Pessoa pessoa_2("Maria", "555-1111", 21);

   pessoa_1.setNome("Joao");
   pessoa_1.setMatricula("14/0078070");
   pessoa_1.setTelefone("555-5555");
   pessoa_1.setSexo("M");
   pessoa_1.setIdade(20);
 
   //cout << "Nome: " << pessoa_1.nome; << endl;
   cout << "Nome: " << pessoa_1.getNome() << endl;
   cout << "Matrícula: " << pessoa_1.getMatricula() << endl;
   cout << "Telefone: " << pessoa_1.getTelefone() << endl;
   cout << "Sexo: " << pessoa_1.getSexo() << endl;
   cout << "Idade: " << pessoa_1.getIdade() << endl;

   cout << endl;
   cout << "Nome: " << pessoa_2.getNome() << endl;
   cout << "Telefone: " << pessoa_2.getTelefone() << endl;
   cout << "Idade: " << pessoa_2.getIdade() << endl;

   pessoa_1.imprimeDados();
   pessoa_2.imprimeDados();

   return 0;
}
